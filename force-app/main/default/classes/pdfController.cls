public with sharing class pdfController {
    @AuraEnabled
    public static void processOpportunityId(Id opportunityId, String urlPdf) {
        System.debug('Received Opportunity ID: ' + opportunityId);
        System.debug('Received urlPdf: ' + urlPdf);

        PageReference pdf = new PageReference(urlPdf);
        pdf.setRedirect(true);
        Blob pdfBlob = pdf.getContentAsPDF();
        System.debug(pdfBlob);

        ContentVersion contentVersion = new ContentVersion();
        contentVersion.ContentLocation = 'S'; 
        contentVersion.VersionData = pdfBlob;
        contentVersion.Title = 'Opp ' + opportunityId + '.pdf';
        contentVersion.PathOnClient = 'Opp ' + opportunityId + '.pdf';
        insert contentVersion;
        System.debug(contentVersion);

        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:contentVersion.Id].ContentDocumentId;

        // Create a new ContentDocumentLink object to link the file to the Opportunity object
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.ContentDocumentId = conDoc;
        contentDocumentLink.LinkedEntityId = opportunityId;
        contentDocumentLink.ShareType = 'V';
        insert contentDocumentLink;
        System.debug(contentDocumentLink);


        // OpDocument__c opDocument = new OpDocument__c();
        // opDocument.Opportunity__c = opportunityId;
        // opDocument.ContentVersionDocumentId__c = contentVersion.Id; // Store Id in Text field
        // // opDocument.Document__c = pdfBlob;
        // insert opDocument;
        // System.debug(opDocument);
    }
}
