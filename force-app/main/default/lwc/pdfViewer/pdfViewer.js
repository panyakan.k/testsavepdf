import { LightningElement, api } from 'lwc';
import processOpportunityId from '@salesforce/apex/pdfController.processOpportunityId';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';

export default class PdfViewer extends LightningElement {
    @api recordId;
    urlPdf = 'https://mindful-wolf-8pe85l-dev-ed--c.trailblaze.vf.force.com/apex/testPdf';

    connectedCallback() {
        this.urlPdf;
    }

    async savePdf() {
        try {
            await this.processOpportunity();
            this.showSuccessToast();
            this.closeQuickAction();
        } catch (error) {
            this.showErrorToast(error);
        }
    }

    async processOpportunity() {
        try {
            await processOpportunityId({ 
                opportunityId: this.recordId,
                urlPdf: this.urlPdf
            });
            console.log('Opportunity Id processed successfully');
        } catch(error) {
            console.error('Error processing Opportunity Id:', error);
            throw error;
        }
    }

    showSuccessToast() {
        const evt = new ShowToastEvent({
            title: 'Success',
            message: 'PDF processed successfully.',
            variant: 'success',
        });
        this.dispatchEvent(evt);
    }

    showErrorToast(error) {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: 'Error processing PDF: ' + error.body.message,
            variant: 'error',
        });
        this.dispatchEvent(evt);
    }

    closeQuickAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }
}
